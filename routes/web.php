<?php
header("Access-Control-Allow-Origin: *");




    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */
    Route::group(['middleware' => 'client_guest'], function () {
        Route::get('/', function () {
            return view('client.home');
        });
    });

    Route::get('/faq', function () {
        return view('client.faq');
    });
    Route::get('/terms', function () {
        return view('client.termsandcondition');
    });
    Route::get('/privacy', function () {
        return view('client.Privacypolicy');
    });


    /******************  START  CLIENT ROUTE::    ***********************************************/

    Route::group(['prefix' => 'client', 'namespace' => 'Client\Auth', 'middleware' => 'client_guest'], function () {
        Route::get('/register', 'RegisterController@showRegistrationForm')->name('client.register');
        Route::post('/register', 'RegisterController@register')->name('client.register');
        Route::get('/login', 'LoginController@showLoginForm')->name('client.login');
        Route::post('/login', 'LoginController@postlogin')->name('client.login');
        Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('client.password.request');
        Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('client.password.email');
        Route::get('/password/reset/{id}', 'ResetPasswordController@showResetForm')->name('client.password.reset');
        Route::post('/password/reset/{id}', 'ResetPasswordController@reset')->name('client.password.update');
    });

    Route::group(['prefix' => 'client', 'namespace' => 'Client', 'middleware' => 'client_auth'], function () {
        Route::get('/logout', 'Auth\LoginController@logout')->name('client.logout');
        Route::get('/payment', 'HomeController@index');
        Route::get('/home', 'HomeController@clienthome');
        Route::get('/portfolio', 'HomeController@portfolio');
        Route::get('/verification', 'HomeController@verification');
        Route::get('/history', 'HomeController@history');
        Route::get('/help', 'HomeController@help');
        Route::get('/profile', 'HomeController@profile');
        Route::get('/changepassword', 'HomeController@changepassword');
        Route::post('/changepassword', 'HomeController@savepassword');
        Route::post('/profile', 'HomeController@saveprofile');
        Route::post('/saveprofileimage', 'HomeController@saveprofileimage');
        Route::get('/paytmpayment', 'HomeController@paytmpayment');
        Route::post('paytmpayment', 'HomeController@order');
        Route::post('scriptsave', 'HomeController@scriptsave');
        Route::post('saverazorpayresponse', 'HomeController@saveRazorPayResponse');
        Route::get('title/{file_type?}', 'HomeController@scriptType');
        Route::get('files/{title}', 'HomeController@filesTitle');
      
    });
    /******************  END  CLIENT ROUTE::    ***********************************************/


    /******************  START ADMIN ROUTE::    ***********************************************/
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin\Auth', 'middleware' => 'guest'], function () {
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login');
        Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'RegisterController@register');
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');
    });

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin\Auth', 'middleware' => 'auth'], function () {
        Route::get('email/verify', 'VerificationController@show')->name('verification.notice');
        Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');
        Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');
        Route::get('/logout', 'LoginController@logout')->name('admin.logout');
    });
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'verified']], function () {
        Route::get('/home', 'HomeController@index');
        Route::get('/profile', 'HomeController@profile');
        Route::post('/profile', 'HomeController@saveprofile');
        Route::resource('/users', 'UserController');
        Route::resource('/roles', 'RoleController');
        Route::resource('clients', 'ClientsController');
        Route::resource('clientregisterations', 'ClientregisterationsController');
        Route::get('clientregisterations/viewsallfiles/{id}', 'ClientregisterationsController@viewsallfiles');
        Route::resource('scriptsfolders', 'ScriptsfoldersController');
    });

/******************  END ADMIN ROUTE::    ***********************************************/
