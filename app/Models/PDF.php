<?php

namespace App\Models;

use Codedge\Fpdf\Facades\Fpdf;

class PDF extends Fpdf
{

    function Header()
    {
        // Logo
        Fpdf::Image('logo.png',10,6,30);

        // Arial bold 15
        Fpdf::SetFont('Arial','B',15);

        // Move to the right
        Fpdf::Cell(80);

        // Title
        Fpdf::Cell(30,10,'Title',1,0,'C');

        // Line break
        Fpdf::Ln(20);

    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        Fpdf::SetY(-15);

        // Arial italic 8
        Fpdf::SetFont('Arial','I',8);

        // Page number
        Fpdf::Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
}
