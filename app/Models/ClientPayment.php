<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientPayment extends Model
{

    protected $table = 'client_payment';


    protected $primaryKey = 'id';

    protected $fillable = ['client_id', 'order_id', 'amount', 'transaction_id', 'payment_status'];

    public function client(){
        return $this->belongsTo('App\Client','client_id');
    }
}
