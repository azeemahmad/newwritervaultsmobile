<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Clientregisteration extends Model
{
     use SoftDeletes;

     use  HasRoles;
    protected $table = 'clientregisterations';


    protected $primaryKey = 'id';


    protected $fillable = ['client_id', 'auther_name', 'co_auther_number', 'file_type', 'title', 'file1', 'file2', 'file3', 'status','order_total'];

    public function client(){
        return $this->belongsTo('App\Client','client_id');
    }


}
