<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Clientregisteration;
use Illuminate\Http\Request;
use App\Authorizable;

class ClientregisterationsController extends Controller
{
     use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $clientregisterations = Clientregisteration::where('client_id', 'LIKE', "%$keyword%")
                ->orWhere('auther_name', 'LIKE', "%$keyword%")
                ->orWhere('co_auther_name', 'LIKE', "%$keyword%")
                ->orWhere('file_type', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('file1', 'LIKE', "%$keyword%")
                ->orWhere('file2', 'LIKE', "%$keyword%")
                ->orWhere('file3', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->groupBy('client_id')->paginate($perPage);
        } else {
            $clientregisterations = Clientregisteration::latest()->groupBy('client_id')->paginate($perPage);
        }

        return view('admin.clientregisterations.index', compact('clientregisterations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.clientregisterations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'client_id' => 'required',
			'auther_name' => 'required',
			'file_type' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('file1')) {
            $requestData['file1'] = $request->file('file1')
                ->store('uploads', 'public');
        }
        if ($request->hasFile('file2')) {
            $requestData['file2'] = $request->file('file2')
                ->store('uploads', 'public');
        }
        if ($request->hasFile('file3')) {
            $requestData['file3'] = $request->file('file3')
                ->store('uploads', 'public');
        }

        Clientregisteration::create($requestData);

        return redirect('admin/clientregisterations')->with('flash_message', 'Clientregisteration added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $clientregisteration = Clientregisteration::findOrFail($id);

        return view('admin.clientregisterations.show', compact('clientregisteration'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $clientregisteration = Clientregisteration::findOrFail($id);

        return view('admin.clientregisterations.edit', compact('clientregisteration'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'client_id' => 'required',
			'auther_name' => 'required',
			'file_type' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('file1')) {
            $requestData['file1'] = $request->file('file1')
                ->store('uploads', 'public');
        }
        if ($request->hasFile('file2')) {
            $requestData['file2'] = $request->file('file2')
                ->store('uploads', 'public');
        }
        if ($request->hasFile('file3')) {
            $requestData['file3'] = $request->file('file3')
                ->store('uploads', 'public');
        }

        $clientregisteration = Clientregisteration::findOrFail($id);
        $clientregisteration->update($requestData);

        return redirect('admin/clientregisterations')->with('flash_message', 'Clientregisteration updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Clientregisteration::destroy($id);

        return redirect('admin/clientregisterations')->with('flash_message', 'Clientregisteration deleted!');
    }

    public function viewsallfiles($id){
        $clientRegistartion=Clientregisteration::where('client_id',$id)->paginate(25);
        return view('admin.clientregisterations.allfiles',compact('clientRegistartion'));

    }
}
