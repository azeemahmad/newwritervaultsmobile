<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Http\Adapter\Guzzle6\Client;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use PaytmWallet;
use App\Models\ClientPayment;
use App\Models\Clientregisteration;
use App\Models\PDF;
use Codedge\Fpdf\Facades\Fpdf;
use Razorpay\Api\Api;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use setasign\Fpdi\Fpdi;
use DB;
use DateTime;
use QrCode;

class HomeController extends Controller
{

  //    public function __construct()
  //    {
  //        $this->middleware('client_auth');
  //    }

  function __construct()
  {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: *");
    header("Access-Control-Allow-Headers: *");
  }

  public function index()
  {

    $client=Auth::guard('client')->user();

    return view('client.payment',compact('client'));
  }
  public function clienthome()
  {

    $cost_data = DB::table('upload_cost')->select('cost')->where('active',1)->first()->cost;

    $client=Auth::guard('client')->user();

    // $title = Title::where('client_id',$client->id)->get();
    return view('client.clienthome',compact('client','title','cost_data'));
  }
  public function portfolio(){
    $client=Auth::guard('client')->user();
    return view('client.portfolio',compact('client'));
  }
  public function verification(){
    $client=Auth::guard('client')->user();
    return view('client.verification',compact('client'));
  }
  public function help(){
    $client=Auth::guard('client')->user();
    return view('client.help',compact('client'));
  }
  public function history()
  {
    $client=Auth::guard('client')->user();
    $orders = Clientregisteration::where('client_id',$client->id)->where('order_status','PAYMENT_COMPLETED')->get();
    return view('client.history',compact('client','orders'));
  }
  public function changepassword(){
    $client=Auth::guard('client')->user();
    return view('client.changepassword',compact('client'));
  }
  public function savepassword(Request $request){
    $this->validate($request, [
      'password' => 'required|string|min:6|confirmed|different:old_password',
      'old_password' => 'required|string|min:6'
    ]);
    $client=Auth::guard('client')->user();
    if (!Hash::check($request['old_password'], $client->password)) {
      return redirect()->back()->with('error_password', 'Old Password not Match from Database!');
    }
    $data=$request->all();
    $password=bcrypt($data['password']);
    $client->password=$password;
    $client->save();
    return redirect()->back()->with('success_password','password changed successfully');
  }
  public function saveprofile(Request $request)
  {
    $user_list = Auth::guard('client')->user();

    if ($request['password'] == null && $request['password_confirmation'] == null && $request['old_password'] == null) {
      $this->validate($request, [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:clients,email,' . Auth::guard('client')->user()->id,
      ]);
    } else {
      if (!Hash::check($request['old_password'], $user_list->password)) {
        return redirect()->back()->with('error_password', 'Old Password not Match from Database!');
      }

      $this->validate($request, [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:clients,email,' . Auth::guard('client')->user()->id,
        'password' => 'required|string|min:6|confirmed|different:old_password',
        'old_password' => 'required|string|min:6'
      ]);
    }
    $data = $request->all();

    if ($request['password'] == null && $request['password_confirmation'] == null && $request['old_password'] == null) {
      if ($request->hasFile('image')) {
        $filename = $this->getFileName($request->image);
        $request->image->move(base_path('public/images/profile_image'), $filename);

        $user_list->update([
          'name' => $data['name'],
          'email' => $data['email'],
          'image' => $filename
        ]);
      } else {
        $user_list->update([
          'name' => $data['name'],
          'email' => $data['email'],
        ]);
      }

    } else {

      if ($request->hasFile('image')) {
        $filename = $this->getFileName($request->image);
        $request->image->move(base_path('public/images/profile_image'), $filename);

        $user_list->update([
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => bcrypt($data['password']),
          'image' => $filename
        ]);

      } else {
        $user_list->update([
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => bcrypt($data['password'])
        ]);
      }
    }
    return redirect('/client/profile')->with('flash_message', 'Profile updated Successfully!');
  }
  protected function getFileName($file)
  {
    return str_random(32) . '.' . $file->extension();
  }

  public function saveprofileimage(Request $request){

    $client=Auth::guard('client')->user();
    if($request->hasFile('profile_image')) {
      $filename = $this->getFileName($request->profile_image);
      $request->profile_image->move(base_path('public/images/ClientProfile'), $filename);
      $client->image=$filename;
      $client->save();
      return 1;
    }
    else{
      return 0;
    }
  }

  public function paytmpayment(Request $request){
    $client=Auth::guard('client')->user();
    return view ('client.paytmpayment',compact('client'));
  }
  public function order(Request $request)
  {

    $client=Auth::guard('client')->user();
    $this->validate($request, [
      'name' => 'required',
      'mobile' => 'required|numeric|digits:10|unique:clients,mobile,' .$client->id,
      'address' => 'required',
      'amount' => 'required',
    ]);

    $input = $request->all();
    $client->mobile=$request->mobile;
    $client->address=$request->address;
    $client->save();

    $data['client_id']=$client->id;
    $data['amount']=$input['amount'];
    $data['order_id']=$request->mobile.rand(1000,9999);
    ClientPayment::create($data);
    $payment = PaytmWallet::with('receive');
    $payment->prepare([
      'order' => $data['order_id'],
      'user' => $client->id,
      'mobile_number' => $input['mobile'],
      'email' => $client->email,
      'amount' => $input['amount'],
      'callback_url' => url('/api/payment/status')
    ]);
    return $payment->receive();
  }


  /**
  * Obtain the payment information.
  *
  * @return Object
  */
  public function paymentCallback()
  {

    $transaction = PaytmWallet::with('receive');
    $response = $transaction->response();
    $order_id = $transaction->getOrderId();
    if($transaction->isSuccessful()){
      ClientPayment::where('order_id',$order_id)->update(['payment_status'=>1, 'transaction_id'=>$transaction->getTransactionId()]);
      return redirect('/client/paytmpayment')->with('success_payment','Amount Added Successfully');

    }else if($transaction->isFailed()){
      ClientPayment::where('order_id',$order_id)->update(['payment_status'=>0, 'transaction_id'=>$transaction->getTransactionId()]);
      return redirect('/client/paytmpayment')->with('error_payment','Amount Added Unsuccessful');


    }
  }

  public function scriptsave(Request $request)
  {
    $first_name = !empty(Auth::guard('client')->user()->first_name)?Auth::guard('client')->user()->first_name:'';
    $last_name = !empty(Auth::guard('client')->user()->last_name)?Auth::guard('client')->user()->last_name:'';

    $no_of_files = 0;

    $data=$request->all();

    if ($request->hasFile('file1'))
    {
      $filename1 = $this->getFileName($request->file1);
      $request->file1->move(base_path('public/UserScripts/'.$data['file_type']), $filename1);
      $data['file1'] = $filename1;
      $no_of_files++;
    }
    if ($request->hasFile('file2'))
    {
      $filename2 = $this->getFileName($request->file2);
      $request->file2->move(base_path('public/UserScripts/'.$data['file_type']), $filename2);
      $data['file2'] = $filename2;
      $no_of_files++;
    }
    if ($request->hasFile('file3'))
    {
      $filename3 = $this->getFileName($request->file3);
      $request->file3->move(base_path('public/UserScripts/'.$data['file_type']), $filename3);
      $data['file3'] = $filename3;
      $no_of_files++;
    }

    $co_author = !empty($data['co_auther_number']) ? $data['co_auther_number'] : false;


    $cost_data = DB::table('upload_cost')->select('cost')->where('active',1)->first();

    $cost                   =   !empty($cost_data->cost) ? $cost_data->cost : 1000.0;        //default Rs. 1000
    $order_total            =   $cost * $no_of_files;
    $data['order_total']    =   $order_total;

    $savedata = Clientregisteration::create($data);
    // dd($savedata);
    if($savedata!=null && $savedata!=false)
    {
      // curlPost

      $body = [
        'amount'    =>  $order_total*100,
        'currency'  =>  'INR',
        'receipt'   =>  $savedata->id
      ];
      $rp_key_id = ENV('RAZOR_PAY_KEY_ID');
      $rp_secret = ENV('RAZOR_PAY_KEY_SECRET');
      $rp_base_url =  ENV('RAZOR_PAY_BASE_URL');

      $endpoint = 'https://'. $rp_key_id.':'.$rp_secret.'@'.$rp_base_url.'orders';

      $airpay_res_json = $this->curlPost($endpoint,$body);

      // dd($airpay_res_json);

      if($airpay_res_json!=false && $airpay_res_json!=null)
      {
        $airpay_res = json_decode($airpay_res_json,true);
        if(array_key_exists('id',$airpay_res))
        {
          $razorpay_order_id = $airpay_res['id'];
          Clientregisteration::where('id', $savedata->id)->update(['razorpay_order_id'=>$razorpay_order_id,'razorpay_response_order_create'=>$airpay_res_json,'order_status'=>'ORDER_CREATED']);
          return json_encode(['status'=>1,'razorpay_order_id'=>$razorpay_order_id]);
        }
      }
    }
    return json_encode(['status'=>0,'razorpay_order_id'=>'']);
  }


  public function saveRazorPayResponse(Request $request)
  {
    $post_data = $request->only(['razorpay_payment_id','razorpay_order_id','razorpay_signature']);
    if($post_data['razorpay_payment_id'] != null && $post_data['razorpay_order_id'] != null && $post_data['razorpay_signature'] != null)
    {
      $first_name = !empty(Auth::guard('client')->user()->first_name)?Auth::guard('client')->user()->first_name:'';
      $last_name = !empty(Auth::guard('client')->user()->last_name)?Auth::guard('client')->user()->last_name:'';
      $membership_no = Auth::guard('client')->user()->membership_no;

      $footer_line_left = "Registered with www.writersvault.io on a secure blockchain";
      $footer_line_right = $first_name." ".$last_name;


      Clientregisteration::where('razorpay_order_id',$post_data['razorpay_order_id'])->update(['order_status'=>'PAYMENT_COMPLETED','razorpay_response_payment'=>json_encode($post_data)]);

      $order = Clientregisteration::where('razorpay_order_id',$post_data['razorpay_order_id'])->first();


      $certificate_line = "This is to certify writersvault.io has registered this script titled ".$order->title." written by ".$first_name." ".$last_name." whose WritersVault.io Membership no is ".$membership_no." on ".date("Y-m-d H:i:s")." and as a proof thereof is placed below writersvault.io's digital signature and seal with relevant details in the QR code (Razar Pay) Reference No: ".$order->razorpay_order_id." and Order Id: ".$order->id;

      $qr_content = "Title: ".$order->title." Order Id: ".$order->id." Writer: ".$first_name." ".$last_name." Membership no: ".$membership_no." Razar Pay Reference No: ".$order->razorpay_order_id." Date: ".date("Y-m-d H:i:s");

      if($order->co_auther_number!=null)
      {
        $co_author = \App\Client::where('membership_no',$order->co_auther_number)->first();
        if($co_author!=null && $co_author!=false)
        {
          $footer_line_right.= " & ".$co_author->first_name." ".$co_author->last_name;
          $certificate_line = "This is to certify writersvault.io has registered this script titled ".$order->title." written by ".$first_name." ".$last_name." and ".$co_author->first_name." ".$co_author->last_name." whose WritersVault.io Membership no are ".$membership_no." and ".$co_author->membership_no." respectively on ".date("Y-m-d H:i:s")." and as a proof thereof is placed below writersvault.io's digital signature and seal with relevant details in the QR code (Razar Pay) Reference No: ".$order->razorpay_order_id." and Order Id: ".$order->id;

          $qr_content = "Title: ".$order->title." Order Id: ".$order->id." Writer: ".$first_name." ".$last_name." Membership no: ".$membership_no."
          Co-Writer: ".$co_author->first_name." ".$co_author->last_name." Membership No:".$co_author->membership_no." Razar Pay Reference No: ".$order->razorpay_order_id." Date: ".date("Y-m-d H:i:s");
        }
      }

      $pdf = new FPDI();
      // add a page
      // set the source file
      $file1 =  $order->file1;
      $file2 =  $order->file2;
      $file3 =  $order->file3;

      $files = [$file1,$file2,$file3];

      $file_type =  $order->file_type;
      $stampname=asset('stamp.png');
      $qr_file_path = public_path('UserQR/qr_'.substr(md5(mt_rand()), 0, 15).'.png');

      $qrCode=QrCode::size(100)->format('png')->encoding('UTF-8')->generate($qr_content,$qr_file_path);
      $i=1;
      foreach ($files as $fi)
      {
        if($fi!=null)
        {


          $pdf->setSourceFile('UserScripts/'.$file_type.'/'.$fi);
          $pageCount = $pdf->setSourceFile('UserScripts/'.$file_type.'/'.$fi);
          // iterate through all pages
          for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++)
          {
            $pdf->SetFont('Helvetica');
            $pdf->SetFillColor(200,200,200);
            // import a page
            $templateId = $pdf->importPage($pageNo);
            // get the size of the imported page
            $size = $pdf->getTemplateSize($templateId);

            // create a page (landscape or portrait depending on the imported page size)
            $pdf->AddPage('P', array($size['width'], $size['height']));

            // use the imported page
            $pdf->useTemplate($templateId);

            // $pdf->SetFont('Helvetica');
            // $pdf->SetXY(5, 5);
            // $pdf->Write(8, 'MAD HEADER');

            $pdf->Image(url('green_logo.png'), 5, 5, 30, '', '', url('/'));
            $pdf->SetXY(5,-30);


            $pdf->MultiCell(0, 8, $footer_line_left, 0 ,'L' ,1);

            $pdf->SetXY(-90,-30);

            $pdf->MultiCell(0, 8,$footer_line_right, 0 ,'R' ,1);
            $i++;
          }

          /**
          *   Certificate Page Start
          */
//          $pdf->AddPage();
//          $pdf->Rect(5, 5, $size['width']-15, $size['height']-10);
//
//
//          /* heading */
//          $pdf->SetFont('Helvetica','BU',18);
//          $pdf->MultiCell($size['width']-10, 10, 'Certificate of Registration' ,0 ,'C');
//
//
//          $pdf->SetFont('Helvetica','',12);
//          $pdf->SetXY(5,60);
//          $pdf->MultiCell($size['width']-10, 10, $certificate_line ,0 ,"C");
//
//          /**
//          *   Certificate Page End
//          */
//          $pdf->Output('UserScripts/'.$file_type.'/'.$fi, "F");



          $pdf->AddPage();
          $pdf->SetFillColor(255, 255, 255);
          $pdf-> SetAutoPageBreak('on',0);
          $pdf->SetFont('Helvetica','B',14);
          $pdf->SetTextColor(255, 0, 0);
          $pdf->MultiCell(0, 6, 'Certificate of Registration' , 0 , 'C' , 1);
          $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
          $pdf->SetFont('Helvetica','B',10);
          $pdf->SetTextColor(0, 0, 0);
          $pdf->MultiCell(0, 6, 'This is to certify that I have registered this Script', 0 , 'C' , 1);
          $pdf->MultiCell(0, 6, 'titled  '. $order->title, 0 , 'C' , 1);
          if(isset($co_author) && $co_author!=null && $co_author!=false){
            $pdf->MultiCell(0, 6, 'Written by '.$first_name." ".$last_name.' with '.$co_author->first_name." ".$co_author->last_name. 0 , 'C' , 1);
            $pdf->MultiCell(0, 6, 'Whose Writer Vault Membership No. are '. $co_author->membership_no  , 0 , 'C' , 1);
          }
          else {
            $pdf->MultiCell(0, 6, 'Written by '. $first_name." ".$last_name , 0 , 'C' , 1);
            $pdf->MultiCell(0, 6, 'Whose Writer Vault Membership No. is '. $first_name." ".$last_name  , 0 , 'C' , 1);
          }
          $pdf->MultiCell(0, 6, 'On '.date('d/m/Y'), 0 , 'C' , 1);
          // $pdf->MultiCell(0, 6, 'On '.$date , 0 , 'C' , 1);
          $pdf->MultiCell(0, 6, '& as a proof thereof is placed below my digital signature and'  , 0 , 'C' , 1);
          $pdf->MultiCell(0, 6, ' seal of the Association with relevant details in the QR code.'  , 0 , 'C' , 1);
          $pdf->MultiCell(0, 6, '(Razar Pay) Reference No.:'. $order->razorpay_order_id.' and Order Id:'.$order->id, 0 , 'C' , 1);
          $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
          $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);

          //$pdf->Image('images/pdflogo.png',2, 2,15);
          // Arial bold 15

          $pdf->SetFont('Helvetica','B',10);
          $pdf->SetTextColor(0, 0, 100);
          // $pdf->MultiCell(0, 6, 'Mr ABC ', 0 , 'R' , 1);
          // $pdf->MultiCell(0, 6, '(General Secretary WV)', 0 , 'R' , 1);
          $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
          $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
          $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
          $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
          $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
          $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
          $pdf->MultiCell(0, 6,' ', 0 ,'R' ,1);
          $pdf->Image($stampname,140, 75, 75, 75);
          $pdf->SetFont('Helvetica','B',9);
          $pdf->MultiCell(0, 6, 'Note - This certificate is subject to the declaration by the writers that This work is our original creation. We  hereby declare that we have neither read it anywhere nor watched it in any Film/TV show. In case it is found otherwise we understand that our registration of this work will automatically stand cancelled and we will be solely responsible for the consequences whatsoever.', 0 , 'L' , 1);
          $pdf->SetFont('Helvetica','B',8);
          $pdf->SetTextColor(255, 0, 0);
          $pdf->MultiCell(0, 6, 'Tampering with document cancels the digital signature & thus the registration.', 0 , 'C' , 1);
          // dd($qrCode);
          $pdf->Image($qr_file_path,75, 175, 50, 50);
          $pdf->SetY(-20);

          $pdf->SetFont('Helvetica','I',8);
          $pdf->SetFillColor(128,128,128);
          $pdf->SetTextColor(0, 0, 0);
          $pdf->SetFont('Helvetica', '', 10);
          $pdf->SetXY(5,-30);
          $pdf->MultiCell(0, 8, $footer_line_left, 0 ,'L' ,1);
          $pdf->SetXY(-90,-30);
          $pdf->MultiCell(0, 8,$footer_line_right, 0 ,'R' ,1);
          $pdf->Output('UserScripts/'.$file_type.'/'.$fi, "F");
        }
      }
      return 1;
    }
    else
    {
      return 0;
    }
  }

  function scriptType($file_type = null)
  {
    $client = Auth::guard('client')->user();
    $files = Clientregisteration::where('order_status','PAYMENT_COMPLETED')->where('client_id',$client->id);

    if($file_type!=null)
    {
      $files->where('file_type',$file_type);
    }
    $files = $files->get();
    return view('client.title',compact('client','files','file_type'));
  }

  function  filesTitle($title)
  {
    $client = Auth::guard('client')->user();
    $files = Clientregisteration::where('order_status','PAYMENT_COMPLETED')->where('client_id',$client->id)->where('title', urldecode($title))->get();
    return view('client.files',compact('client','files'));
  }

  function curlPost($endpoint,$body)
  {
    // dd($endpoint,$body);
    try {

      $ch = curl_init();
      // dd($ch);
      curl_setopt($ch, CURLOPT_URL,$endpoint);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,$body);

      curl_setopt($ch, CURLINFO_HEADER_OUT, true);

      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $server_output = curl_exec($ch);
      // dd(curl_error($ch));
      return $server_output;
      // $information = curl_getinfo($ch);

      // print_r('<pre>');
      // print_r($information);

      curl_close ($ch);
      // dd($server_output);

    } catch (\Exception $e) {
      // dd($e);
      return false;
    }
  }


}
