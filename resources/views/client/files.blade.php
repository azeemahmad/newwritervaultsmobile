@extends('client.layouts.master')
<style>
.help-block{
  color: red;
}
</style>
@section('content')
  <main id="main">

    <!--==========================
    Why Us Section
    ============================-->
    <section id="why-us" class="outer custom-outer">

      @include('client.layouts.sub_header')

      <div class="container">
        <div class="row padding-tb">
          @include('client.layouts.side_menu')
          {{-- PUT MAIN CONTENT HERE START --}}

          <div class="col-lg-9 custom-right-block-outer">

      <!-- MultiStep Form -->
      <div class="row padding-lr-15">
          <div class="custom-right-block">

            <div class="row">
              <div class="col-lg-12 display-flex">
                <div class="col-lg-9">
                  <h4 class="right-title">Titles</h4>
                  <h5 class="rsub-title">All your works/drafts around one title.</h5>
                </div>

                </div>
            </div>

            <div class="divider"></div>

              <div id="content" class="tab-content" role="tablist">
                  <div id="portfolio" class="card tab-pane fade show active custom-search-outer" role="tabpanel" aria-labelledby="registration">

                    <div id="collapse-A" class="collapse show" role="tabpanel" aria-labelledby="heading-A">
                      <div class="card-body">
                        <div class="container no-padding">

                          <div class="wrap customsearch">
                             <div class="search">
                                <input type="text" class="searchTerm" placeholder="Search your files by keyword">
                                <button type="submit" class="searchButton">
                                  <div class="scircle"></div><i class="fa fa-search"></i>
                               </button>
                             </div>
                             <p>0 filters applied</p>
                         </div>



                            <table class="table table-fluid borderless" id="portfoliotable" border="0">
                            <thead>
                            <tr>
                              <th>Title</th>
                              <th><label class="">Date Registered</label></th>
                              <th><label>Download</label></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($files as $file)
                              {{-- {{ dd($file)}} --}}
                            <tr>
                              <td class="td-odd"><label>{{ $file->title }}</label></td>
                              <td class="td-even"><label>{{ $file->created_at }}</label></td>
                              <td class="td-odd"><label><a Download href="{{ url('UserScripts/'.$file->file_type.'/'.$file->file1) }}">Download</a></label></td>
                            </tr>
                            @endforeach
                            </tbody>
                            </table>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
          </div>
      </div>

    </div>

          {{-- PUT MAIN CONTENT HERE END --}}
        </div>
      </div>

      <div class="container">
        <div class="form-footer1 col-lg-3 offset-9">
          <div class=""><a href="#">Terms of use</a></div>
          <div class=""><a href="#">Privacy policy</a></div>
        </div>
      </div>


    </section>
  </main>
@endsection
