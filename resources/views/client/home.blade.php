@extends('client.layouts.master')
<style>
    .help-block{
        color: red;
    }
</style>
@section('content')
<!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix homepage-intro">
    <div class="container d-flex h-100">
      <div class="row justify-content-center align-self-center">
        <div class="col-md-7 intro-info order-md-first order-last">
          <h2>Secure your creation on paper, the safest way possible - through blockchain.</h2>
          <p>Freedom with dignity and agility with security which the guilds can’t offer!</p>
          <div>
            <a href="{{ url('client/register') }}" class="btn-get-started scrollto">Book Vault Space</a>
          </div>
        </div>

        <div class="col-md-5 intro-img order-md-last order-first visibility-hidden">
          <img src="https://dummyimage.com/500x400/000/fff" alt="" class="img-fluid">
        </div>
      </div>

    </div>
  </section>

  <main id="main">

    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg homepage-services">
      <div class="container">

        <header class="section-header">
          <h3>Why choose us?</h3>
          <!-- <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus.</p> -->
        </header>

        <div class="row">

          <div class="col-md-4 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="" style="">
                <img src="img/why1.png">
              </div>
              <h4 class="title"><a href="">Digitally Fingerprinted</a></h4>
              <!-- <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p> -->
            </div>
          </div>
          <div class="col-md-4 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="" style="">
                <img src="img/bchain.png">
              </div>
              <h4 class="title"><a href="">Registered on blockchain</a></h4>
              <!-- <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p> -->
            </div>
          </div>

          <div class="col-md-4 col-lg-4 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="" style="">
                <img src="img/certificate.png">
              </div>
              <h4 class="title"><a href="">A credible certificate of registration</a></h4>
              <!-- <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p> -->
            </div>
          </div>


        </div>

        <div class="row">
          <a href="{{ url('client/register') }}" class="btn">Register</a>
        </div>

      </div>
    </section><!-- #services -->



    <!--==========================
      Clients Section
    ============================-->
    <section id="testimonials">
      <div class="container">

        <header class="section-header">
          <h3>Testimonials</h3>
        </header>

        <div class="row justify-content-center">
          <div class="col-lg-12">


            <div class="owl-carousel wow fadeInUp custom-carousel">
              <div class="custom-item">
                <img src="{{asset('img/testimonials/ananth.jpg')}}" alt="">
                <div class="test-details">
                  <h3>Ananth Vardhan</h3>
                  <!-- <p>Screenwriter</p> -->
                  <p>One no longer needs to be part of an elite writers guild to safeguard their works. writersvault.io is the next-gen solution for writers who are starting out and find it extremely difficult to be part of any writers guild.</p>
                </div>
              </div>
              <div class="custom-item">
                <img src="{{asset('img/testimonials/uday.jpeg')}}" alt="">
                <div class="test-details">
                  <h3>Uday Agamharshan</h3>
                  <p>The solution offered by writersvault.io is in principle the most secure platform to store and secure my work only for my viewing and people I share with than with any guild that doesn't store, only attests and emails to the creator.</p>
                </div>
              </div>
              <div class="custom-item">
                <img src="{{asset('img/testimonials/rakesh.jpg')}}" alt="">
                <div class="test-details">
                  <h3>Rakesh RudraVanka</h3>
                  <p>Its a comfortable way of securing one's work right from the writer's table than go apply for attestation or wait for a particular day to have someone sign and stamp my works manually while I am not sure when the authorized signatory will make himself or herself present. Most of us writers are better off securing our works in the cosy comfort of our couch.</p>
                </div>
              </div>
              <div class="custom-item">
                <img src="{{asset('img/testimonials/kaushik.jpg')}}" alt="">
                <div class="test-details">
                  <h3>Kaushik Subramanyam</h3>
                  <p>I was always afraid of plagiarism and security of my works and then I came across the writersvault.io online and registered my scripts without much effort & at affordable fees and got a digital ID. Now I'm feeling my scripts are very much safer with WritersVault.io and can access anytime, anywhere.</p>
                </div>
              </div>
              <div class="custom-item">
                <img src="{{asset('img/testimonials/amarnath.jpg')}}" alt="">
                <div class="test-details">
                  <h3>Amarnath</h3>
                  <p>Writer's Vault is a hassle-free way for us to register and secure our creative work.  A much-needed alternative to the existing systems in the Indian film industry.</p>
                </div>
              </div>
            </div>

            <div class="testimonial-text">
              <!-- <p>“Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est eopksio laborum. Sed ut perspiciatis unde omnis istpoe natus error sit voluptatem accusantium doloremque eopslo”.
              </p> -->
            </div>


          </div>
        </div>


      </div>
    </section><!-- #testimonials -->


  </main>
@include('client.layouts.footer')
@endsection
@section('scripts')
<script type="text/javascript">
    $('.custom-carousel').owlCarousel({
      autoplay: true,
      center: true,
      loop: true,
      nav: false,
 responsive:{
     0:{
         items: 1,
         center: false,
         dots: true
     },
     600:{
        items: 1,
        center: false,
         dots: true
     },
     1000:{
         
     }
 }
    });

</script>
@endsection
