@extends('client.layouts.master')
<style>
    .help-block{
        color: red;
    }
</style>
@section('content')
    <main id="main">
        <section id="why-us" class="outer">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-lg-6 no-padding">
                        <div class="why-us-img">
                            <img src="{{ url('img/lbanner.png') }}" alt="" class="img-fluid">
                        </div>
                    </div>
                    @if (session('failed_message'))
                        <p class="alert alert-danger">
                            {{ session('failed_message') }}
                        </p>
                    @endif
                    @if (session('flash_message'))
                        <p class="alert alert-success">
                            {{ session('flash_message') }}
                        </p>
                    @endif
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="signup-form-outer">
                                <h2 class="main-title">WRITERS VAULT</h2>
                                <h5 class="sub-title">Please complete to create your account.</h5>
                                <div class="col-md-8 offset-2 mysignup">
                                    <form id="msform" method="POST" action="{{ route('client.register') }}" autocomplete="off">
                                        @csrf
                                            <div class="col-lg-12 no-padding">
                                                <div class="col-lg-6 signup-name first_name">
                                                    <div class="form-group">
                                                        <input type="text" name="first_name" value="{{old('first_name')}}" class="form-control" id="first_name" placeholder="First name" />
                                                        @if ($errors->has('first_name'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                           </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 signup-name last_name">
                                                    <div class="form-group">
                                                        <input type="text" name="last_name" value="{{old('last_name')}}" class="form-control" id="last_name" placeholder="Last name" data-rule="" data-msg="Last name" />
                                                        @if ($errors->has('last_name'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                           </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="user_name" class="form-control" value="{{old('user_name')}}" id="user_name" placeholder="Username" data-rule="" data-msg="Enter Username" />
                                                @if ($errors->has('user_name'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('user_name') }}</strong>
                                                           </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control" value="{{old('email')}}" id="email" placeholder="Email" data-rule="" data-msg="Enter Email" />
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                           </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="password" id="password" placeholder="Password" data-rule="password" data-msg="Please enter a valid Password" />
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                           </span>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password" data-rule="password" data-msg="Please enter a valid Password" />
                                            </div>

                                            <div class="form-group signup-outer">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" required>&nbsp;&nbsp; I agree with terms & conditions.</label>
                                                </div>
                                                <!-- <div class="forgot-password"><a href="#">Forgot Password</a></div> -->
                                            </div>
                                            <input type="submit" name="next" class="action-button" value="Next"/>
                                            <div class="already-user"><a href="{{url('/client/login')}}">Already have an account? Sign in.</a></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="form-footer col-lg-6 offset-lg-3">
                            <div class=""><a href="{{ url('/terms') }}" target="_blank">Terms of use</a></div>
                            <div class=""><a href="{{ url('/privacy') }}" target="_blank">Privacy policy</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
