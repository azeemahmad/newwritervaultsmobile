@extends('client.layouts.master')
<style>
    .help-block{
        color: red;
    }
</style>
@section('content')
    <main id="main" class="verification">

        <!--==========================
          Why Us Section
        ============================-->
        <section id="why-us" class="outer custom-outer">

            <div class="container-fluid page-header-outer">
                <div class="container no-padding">
                    <div class="page-header col-lg-6">
                        <div class="bcircle"></div><span class="ltitle">FAQs</span>
                    </div>

                    <div class="page-header right-block col-lg-5 offset-1">
                        <div class="col-md-3"><div class="scircle"></div><span class="ltitle">Credits</span></div>
                        <div class="col-md-3 no-padding"><div class="scircle"></div><span class="ltitle">Purchase</span></div>
                        <div class="col-md-6 account-nav">
                            <nav class="main-nav float-right d-none d-lg-block no-padding">
                                <ul class="no-padding">
                                    <li class="drop-down"><a href=""><i class="fa fa-user-circle-o" aria-hidden="true"></i>{{Auth::guard('client')->user()->user_name}}</a>
                                        <ul>
                                            <li><a href="{{url('/client/paytmpayment')}}">Add Credits</a></li>
                                            <li><a href="{{url('/client/changepassword')}}">Change Password</a></li>
                                            <li><a href="#">Feedback</a></li>
                                            <li><a href="{{url('/client/logout')}}">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">

                <!-- <header class="section-header">
                  <h3>Why choose us?</h3>
                  <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus.</p>
                </header> -->

                <div class="row padding-tb">

                    <div class="col-lg-3 no-padding">
                        <div class="custom-left-block">

                            <div class="upper-left">
                                <!-- <div class="profile-pic">
                                  <img src="img/dummy.jpg">
                                  <div class="edit"><a href="#"><input type="file" name=""><i class="fa fa-pencil fa-lg"></i></a>
                                  </div>
                                </div> -->
                                <div class="avatar-upload">
                                    <div class="avatar-edit">

                                        <form id="datafiles" action="javascript:void(0)" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <input type='file' id="imageUpload" name="profile_image" accept=".png, .jpg, .jpeg" />
                                            <label for="imageUpload"></label>
                                        </form>
                                    </div>
                                    @php $imageProfile=$client->image;@endphp
                                    <div class="avatar-preview">
                                        <div id="imagePreview">
                                            @if($client->image == null || $client->image ='')
                                                <img src="{{asset('img/dummy.jpg')}}" style="width: 100%;" class="dummyimage" alt="">
                                            @else
                                                <img src="{{env('APP_URL').'/images/ClientProfile/'.$imageProfile}}" style="width: 193px;height: 195px;" class="dummyimage">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <h6>{{Auth::guard('client')->user()->first_name.' '.Auth::guard('client')->user()->last_name}}</h6>
                                <?php
                                   $payment=$client->payment()->where('payment_status',1)->sum('amount');
                                ?>
                                <p>{{isset($payment)?$payment:0}} Credits</p>

                                <a class="update-kyc" href="#"><div class="scircle"></div><span class="ltitle">Update KYC</span></a>
                            </div>

                            <div class="clearfix"></div>

                            <div class="lower-left">
                                <ul id="tabs" class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a id="tab-A" href="{{url('/client/home')}}" class="nav-link"><div class="scircle"></div><label>Registration</label></a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="tab-B" href="{{url('/client/portfolio')}}" class="nav-link"><div class="scircle"></div><label>Portfolio</label></a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="tab-C" href="{{url('/client/verification')}}" class="nav-link"><div class="scircle"></div><label>Verification</label></a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="tab-C" href="{{url('/client/history')}}" class="nav-link"><div class="scircle"></div><label>History</label></a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="tab-C" href="{{url('/client/help')}}" class="nav-link"><div class="scircle"></div><label>Help</label></a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-9 custom-right-block-outer">

                        <!-- MultiStep Form -->
                        <div class="row padding-lr-15">
                            <div class="custom-right-block">
                                <form method="POST" action="{{ url('client/paytmpayment') }}" accept-charset="UTF-8" class="form-horizontal"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="panel-group">
                                        <div class="panel panel-default  panel-primary">
                                            <div class="">
                                                @if (count($errors) > 0)
                                                    <div class="alert alert-danger">
                                                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                                @if (session('error_payment'))
                                                        <span class="alert alert-danger">
                                                     {{ session('error_payment') }}
                                                         </span>
                                                @endif
                                                @if (session('success_payment'))
                                                    <span class="alert alert-success">
                                                    {{ session('success_payment') }}
                                                    </span>
                                                @endif
                                            </div>
                                            <br/>
                                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                                <label for="name" class="col-md-4 control-label">{{ 'Name' }}</label>

                                                <div class="col-md-6">
                                                    <input class="form-control" name="name" type="text" id="name"
                                                           value="{{Auth::guard('client')->user()->first_name.' '.Auth::guard('client')->user()->last_name}}" readonly>
                                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                                <label for="email" class="col-md-4 control-label">{{ 'Email' }}</label>

                                                <div class="col-md-6">
                                                    <input class="form-control" name="email" type="email" id="email"
                                                           value="{{ Auth::guard('client')->user()->email}}" disabled>
                                                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
                                                <label for="mobile"
                                                       class="col-md-4 control-label">{{ 'Mobile' }}</label>

                                                <div class="col-md-6">
                                                    <input class="form-control" name="mobile" type="number"
                                                           id="mobile" value="{{isset(Auth::guard('client')->user()->mobile)?Auth::guard('client')->user()->mobile:''}}">
                                                    {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                                <label for="address"
                                                       class="col-md-4 control-label">{{ 'Address' }}</label>

                                                <div class="col-md-6">
                                                    <textarea class="form-control" placeholder="Address" name="address">{{isset(Auth::guard('client')->user()->address)?Auth::guard('client')->user()->address:''}}</textarea>
                                                    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('amount') ? 'has-error' : ''}}">
                                                <label for="amount"
                                                       class="col-md-4 control-label">{{ 'Amount' }}</label>

                                                <div class="col-md-6">
                                                    <input class="form-control" name="amount" type="number"
                                                           id="amount">
                                                    {!! $errors->first('amount', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-offset-4 col-md-6">
                                                    <input class="btn btn-success" type="submit" value="{{'Pay to PatTM' }}">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>




            </div>



            <div class="container">
                <div class="form-footer1 col-lg-3 offset-9">
                    <div class=""><a href="{{ url('/terms') }}">Terms of use</a></div>
                    <div class=""><a href="{{ url('/privacy') }}">Privacy policy</a></div>
                </div>
            </div>


        </section>
    </main>
@endsection