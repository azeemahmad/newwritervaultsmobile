@extends('client.layouts.master')
<style>
    .help-block{
        color: red;
    }
</style>
@section('content')
    <main id="main">

        <section id="faq">
            <div class="container">
                <header class="section-header">
                    <h3>Terms and Conditions</h3>
                    <h4><strong>I. Introduction</strong></h4>
                </header>

                <ul>
                    <li>
                        You (“<strong>User</strong>” or “<strong>you</strong>”) agree to abide by and be bound by the Terms described herein and by all the Terms, policies, and guidelines incorporated by reference as well as any additional Terms and restrictions presented in relation to specific content or a specific service or feature (“<strong>Terms</strong>”) and the linked Privacy Policy, before you use the applications, websites or any content, product, service, or feature available through the website including the embedded viewer and  all of the content featured or displayed on the Website, including, but not limited to, text, graphics, data, photographic images, moving images, sound, illustrations, software and the selection and arrangement thereof (“<strong>Content</strong>”) made available to you by << <a href="http://writersvault.io/" target="_blank">www.writersvault.io</a> >> (hereinafter referred to as “<strong>Website</strong>” or “<strong>we</strong>” or “<strong>our</strong>” or “<strong>us</strong>”)

                        <br><br>
                        <strong>PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY. BY ACCESSING OR USING THE WEBSITE YOU AGREE TO ABIDE BY AND BE BOUND BY THE TERMS DESCRIBED HEREIN. IF YOU DO NOT AGREE TO ANY OR ALL OF THESE TERMS, DO NOT USE THE WEBSITE.</strong>
                    </li>
                    <br>

                    <li>
                        Unless otherwise indicated, all Content on the Website is owned by us, our licensors or our third-party image partners. All elements of the Website, including the Content, are protected by copyright, trade dress, moral rights, trademark and other laws relating to the protection of intellectual property.
                    </li>
                    <br>
                    <li>We provide the registration services to the User subject to the Terms. </li>
                    <br>
                    <li>All information accessed or viewed by the User is considered confidential and is for only the authorized personal or business purposes.</li>
                    <br>
                    <li>These Terms are effective upon acceptance and governs the relationship between the User and Ajastos Film Technology Labs Pvt ltd. (“<strong>Company</strong>”) and includes its affiliates and subsidiaries, jointly and severally) and also the use of the website including wireless Content or systems (“<strong>Website</strong>”). If the Terms conflict with any other document, the Terms will prevail for the purposes of usage of the Website. </li>
                    <br><br>
                </ul>


                <header class="section-header">
                    <h4><strong>II. Acceptance of Terms</strong></h4>
                </header>

                <ul>
                    <li>A.  The Website is the property of the Company and /or its subsidiaries. By browsing, linking, referencing, using or accessing the Website, the User agrees to these Terms, including agreeing to indemnify and hold harmless the Company from all claims of any nature arising from the access and use of these application/websites by the User. These Terms may be changed at any time at the sole discretion of the Company. These Terms pertain to all Websites of the Company, including websites owned, operated or sponsored by any of the subsidiaries or affiliates of the Company</li>
                    <br>
                    <li>B.  Please read these Terms carefully. These Terms, as modified or amended from time to time, are a binding contract between the Company and the User. If the User visits, uses, or operates at the Website (or any future website operated by the Company), the User accepts these Terms. In addition, when the User uses any current or future Content of the Company or visits or uses any of the Content affiliated with the Company, the User will also be subject to the guidelines and conditions applicable to such service.   </li>
                    <br>
                    <li>C.  The Website takes no responsibility for the registration that are provided by any third-party vendors. </li>
                    <br>
                    <li>D.  This document is published in accordance with the provisions of Rule 3 (1) of the Information Technology (Intermediaries guidelines) Rules, 2011 that require publishing the rules and regulations, privacy policy and Terms of Use for access or usage of the Website.</li>
                    <br>
                    <li>E.  The information provided on this Website includes, but is not limited to, the services provided by the Company and does not render any advice, certifications, guarantees or warranties on the Content (as defined below) relating to the services, which the Company makes available on this Website. User understands and acknowledges to enter into and execute separate agreements, as required by the Company for any of the services availed by the User from the Company.</li>
                    <br>
                    <li>F.  The Company reserves the right to make any changes to the Terms and/or our Privacy Policy as may be deemed necessary or desirable without prior notification to the User. If the Company makes changes to the Terms and Privacy Policy and the User continues to use the Website, the User is impliedly agreeing to the revised Terms and Privacy Policy expressed herein. </li>
                    <br><br>
                </ul>

                <header class="section-header">
                    <h4><strong>1. Rights/Restrictions Relating to Website Content</strong></h4>
                </header>

                <ul>
                    <li>Limitations on Linking and Framing. You are free to establish a hypertext link to our Website so long as the link does not state or imply any sponsorship of your website or service by us or by our Website. However, you shall not, without our prior written permission, frame or inline link any of the content of our Website, or incorporate into another website or other service any of our material, content or intellectual property. </li>
                    <br>
                    <li>Our Right to Use Materials You Submit or Post. When you submit or post any material via the Website, you grant us, and anyone authorized by us, a royalty-free, perpetual, irrevocable, non-exclusive, unrestricted, worldwide license to use, copy, modify, transmit, sell, exploit, create derivative works from, distribute, and/or publicly perform or display such material, in whole or in part, in any manner or medium (whether now known or hereafter developed), for any purpose that we choose. The foregoing grant includes the right to exploit any proprietary rights in such posting or submission, including, but not limited to, rights under copyright, trademark or patent laws that exist in any relevant jurisdiction. Also, in connection with the exercise of these rights, you grant us, and anyone authorized by us, the right to identify you as the author of any of your postings or submissions by name, email address or screen name, as we deem appropriate. You understand that the technical processing and transmission of the Website, including content submitted by you, may involve transmissions over various networks, and may involve changes to the content to conform and adapt it to technical requirements of connecting networks or devices. You will not receive any compensation of any kind for the use of any materials submitted by you.</li>
                    <br>
                    <li>Your Limited Right to Use Website Materials. Unless otherwise indicated, all of the Content featured or displayed on the Site, including, but not limited to, text, graphics, data, photographic images, moving images, sound, illustrations, software and the selection and arrangement thereof is owned by the Company, its licensors or its third-party image partners. The Website is provided solely for your browsing and only for the use it was licensed to you by the Company. You are specifically prohibited from: (a) downloading, copying or re-transmitting any or stealing all of the Website or the Content without, or in violation of, a written license, permission or agreement with Company; (b) using any data mining, robots or similar data gathering or extraction methods; (c) manipulating or otherwise displaying the Website or the Content by using framing or similar navigational technology; (d) registering, subscribing, unsubscribing or attempting to register, subscribe or unsubscribe any party for any product or service if you are not expressly authorised by such party to do so; (e) circumventing, disabling or otherwise interfering with security-related features of the Website or any system resources, services or networks connected to or accessible through the Website; (f) selling, licensing, leasing or in any way commercializing the Website or the Content without specific written authorization from the Company; and (g) using the Website or the Content other than for its intended purpose. Such unauthorized use may also violate applicable laws including without limitation copyright and trademark laws, the laws of privacy and publicity, and applicable communications regulations and statutes. </li>
                    <br>
                    <li>Where enabled, you may embed the Content on a website, blog or social media platform using the embedded viewer (the “<strong>Embedded Viewer</strong>”). Not all of the Content will be available for embedded use, and availability may change without notice. The Company reserves the right in its sole discretion to remove its Content from the Embedded Viewer. Upon request, you agree to take prompt action to stop using the Embedded Viewer and/or the Content. You may only use embedded Content for editorial purposes (meaning relating to events that are newsworthy or of public interest). Embedded Content may not be used: (a) for any commercial purpose (for example, in advertising, promotions or merchandising) or to suggest endorsement or sponsorship; (b) in violation of any stated restriction; (c) in a defamatory, pornographic or otherwise unlawful manner; or (d) outside of the context of the Embedded Viewer. The Company (or third parties acting on its behalf) may collect data related to use of the Embedded Viewer and embedded Content, and reserves the right to place advertisements in the Embedded Viewer or otherwise monetise its use without any compensation to you.</li>
                    <br><br>
                </ul>

                <header class="section-header">
                    <h4><strong>2. Access to Certain Features of Our Site</strong></h4>
                </header>

                <ul>
                    <li>To access certain features of our Website, we may ask you to provide certain demographic information including your gender, year of birth, pin code, state and country. In addition, if you elect to sign-up for a particular feature of the Website, such as programs, discussion forums, blogs, photo- and video-sharing pages or social networking features, you may also be asked to register with us on the form provided and such registration may require you to provide information such as your name and email address. You agree to provide true, accurate, current and complete information about yourself as prompted by the Website's registration form. If we have reasonable grounds to suspect that such information is untrue, inaccurate, or incomplete, we have the right to suspend or terminate your account and refuse any and all current or future use of the Website (or any portion thereof). Our use of any information you provide to us as part of the registration process is governed by the terms of our Privacy Policy.</li>
                    <br><br>
                </ul>

                <header class="section-header">
                    <h4><strong>3. Responsibility for User-Provided Content</strong></h4>
                </header>

                <ul>
                    <li>This Website may include a variety of features, such as discussion forums, blogs, photo and video-sharing pages, email services and social networking features that allow feedback to us and allow users to interact with each other on the Website and post content and materials for display on the Website. This Website may also include other features, such as personalized home pages and email services, that allow users to communicate with third parties. By accessing and using any such features, you represent and agree: (i) that you have read and agree to abide by our Rules; (ii) that you are the owner of any material you post or submit, or are making your posting or submission with the express consent of the owner of the material; (iii) that you are making your posting or submission with the express consent of anyone pictured in any material you post or submit, (iv) that you are 18 years of age or older; (v) that the materials will not violate the rights of, or cause injury to, any person or entity; and (vi) that you will indemnify and hold harmless us, our affiliates, and each of our and their respective directors, officers, managers, employees, shareholders, agents, representatives and licensors, from and against any liability of any nature arising out of or related to any content or materials displayed on or submitted via the Website by you or by others using your username and password. </li>
                    <br>
                    <li>You agree to use the Website for lawful purposes only. You are prohibited from posting or transmitting to or through the Website any unlawful, harmful, threatening, abusive, harassing, defamatory, vulgar, obscene, profane, hateful material, or any other material that could give rise to civil or criminal liability under the law.</li>
                    <br>
                    <li>You acknowledge and agree that we may preserve content and materials submitted by you, and may also disclose such content and materials if required to do so by law or if, in our business judgment, such preservation or disclosure is reasonably necessary to: (a) comply with legal process; (b) enforce these Terms; (c) respond to claims that any content or materials submitted by you violate the rights of third parties; or (d) protect the rights, property, or personal safety of our Site, us, our affiliates, our officers, directors, employees, representatives, our licensors, other users, and/or the public.</li>
                    <br>
                    <li>Responsibility for what is posted on discussion forums, blogs, photo- and video-sharing pages, and other areas on the Website through which users can supply information or material, or sent via any email services that are made available via the Website, lies with each user – you alone are responsible for the material you post or send. We are not responsible for the speech, content, messages, information or files that you or others may transmit, post or otherwise provide on or through the Website.</li>
                    <br>
                    <li>You understand that we have no obligation to monitor any discussion forums, blogs, photo- or video-sharing pages, or other areas of the Website through which users can supply information or material. However, we reserve the right at all times, in our sole discretion, to screen content submitted by users and to edit, move, delete, and/or refuse to accept any content that in our judgment violates these Terms or is otherwise unacceptable or inappropriate, whether for legal or any other reasons.</li>
                    <br><br>
                </ul>

                <header class="section-header">
                    <h4><strong>4. Modified Terms</strong></h4>
                </header>

                <ul>
                    <li>The Company reserves the right at all times to discontinue or modify any of its Terms and/or the Privacy Policy as may be deemed necessary or desirable without prior notification to the User. Further, if the Company makes any changes to the Terms and Privacy Policy and the User continues to use the Website, the User is impliedly agreeing to the Terms and Privacy Policy expressed therein. Any such changes, deletions or modifications shall be effective immediately upon the Company’s posting thereof. Any use of the Website by the User after such notice shall be deemed to constitute acceptance by the User of such modifications </li>
                    <br><br>
                </ul>

                <header class="section-header">
                    <h4><strong>5. Disclaimers and Warranties</strong></h4>
                </header>

                <ul>
                    <li>On our Website, we may provide links and pointers to Internet sites maintained by third parties. Our linking to such third-party websites does not imply an endorsement or sponsorship of such websites, or the information, products or services offered on or through the websites. In addition, neither we nor our parent or subsidiary companies nor any of our respective affiliates operate or control in any respect any information, products or services that third parties may provide on or through the Website or on websites linked to by us on the Website.</li>
                    <br>
                    <li>THE INFORMATION, PRODUCTS AND SERVICES OFFERED ON OR THROUGH THE WEBSITE AND ANY THIRD-PARTY SITES ARE PROVIDED "AS IS" AND WITHOUT WARRANTIES OF ANY KIND EITHER EXPRESS OR IMPLIED. TO THE FULLEST EXTENT PERMISSIBLE PURSUANT TO APPLICABLE LAW, WE DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. WE DO NOT WARRANT THAT THE SITE OR ANY OF ITS FUNCTIONS WILL BE UNINTERRUPTED OR ERROR-FREE, THAT DEFECTS WILL BE CORRECTED, OR THAT ANY PART OF THIS WEBSITE, INCLUDING BULLETIN BOARDS, OR THE SERVERS THAT MAKE IT AVAILABLE, ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. YOU ACKNOWLEDGE THAT ACCESS TO DATA STORED BY YOU OR OTHERS ON THE WEBSITE IS NOT GUARANTEED AND THAT WE SHALL NOT BE RESPONSIBLE TO YOU FOR ANY LOSS OF DATA CAUSED BY YOUR USAGE OR ACCESS TO THE WEBSITEOR ITS UNAVAILABILITY.</li>
                    <br>
                    <li>WE DO NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE WEBSITE OR MATERIALS ON THIS WEBSITE OR ON THIRD-PARTY WEBSITES IN TERMS OF THEIR CORRECTNESS, ACCURACY, TIMELINESS, RELIABILITY OR OTHERWISE. </li>
                    <br>
                    <li>YOU ACKNOWLEDGE THAT THE PROVISIONS OF THIS SECTION ARE A MATERIAL INDUCEMENT AND CONSIDERATION TO US IS ONLY LIMITED TO GRANT THE SERVICES CONTAINED IN THESE TERMS AND TO PROVIDE YOU WITH ACCESS TO THE WEBSITE AND THE MATERIALS ON THIS WEBSITE. </li>
                    <br>
                    <li>You must provide and are solely responsible for all hardware and/or software necessary to access the Website. You assume the entire cost of and responsibility for any damage to, and all necessary maintenance, repair or correction of, that hardware and/or software.</li>
                    <br>
                    <li>You acknowledge that by using the Website, you may incur charges from your wireless carrier, internet service provider or other method of internet or data access, and that payment of any such charges will be your sole responsibility. You agree that your use of the Website will be in accordance with all requirements of your wireless carrier, internet service provider and other method of internet or data access. We do not control network access. Your use of these networks may not be secure and may expose your personal information sent over such networks.</li>
                    <br>
                    <li>The Website is provided for informational purposes only and is not intended for trading or investing purposes. The Website should not be used in any high risk activities where damage or injury to persons, property, environment, finances or business may result if an error occurs. You expressly assume all risk for any such use.</li>
                    <br>
                    <li>Your interactions with companies, organizations and/or individuals found on or through our Website, including any purchases, transactions, or other dealings, and any terms, conditions, warranties or representations associated with such dealings, are solely between you and such companies, organizations and/or individuals and third parties. You agree that we will not be responsible or liable for any loss or damage of any sort incurred as the result of any such dealings. You also agree that, if there is a dispute between users of this Website, or between a user and any third party, we are under no obligation to become involved, and you agree to release us and/or our affiliates from any claims, demands and damages of every kind or nature, known or unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way related to such dispute and/or our Website.</li>
                    <br><br>
                </ul>

                <header class="section-header">
                    <h4><strong>6. Limitation of Liability</strong></h4>
                </header>

                <ul>
                    <li>UNDER NO CIRCUMSTANCES, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE, WILL WE OR OUR SUBSIDIARIES, PARENT COMPANIES OR AFFILIATES OR DIRECTORS OR EMP.OYEES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES THAT RESULT FROM THE USE OF, OR THE INABILITY TO USE, THIS SITE, INCLUDING ITS MATERIALS, PRODUCTS, OR SERVICES, OR THIRD-PARTY MATERIALS, PRODUCTS, OR SERVICES MADE AVAILABLE THROUGH THIS WEBSITE, EVEN IF WE ARE ADVISED BEFOREHAND OF THE POSSIBILITY OF SUCH DAMAGES. YOU SPECIFICALLY ACKNOWLEDGE AND AGREE THAT WE ARE NOT LIABLE FOR ANY DEFAMATORY, OFFENSIVE OR ILLEGAL CONDUCT OF ANY USER. IF YOU ARE DISSATISFIED WITH THE WEBSITE, OR ANY MATERIALS, PRODUCTS, OR SERVICES ON THE SITE, OR WITH ANY OF THE BENSITE'S TERMS AND CONDITIONS, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE WEBSITE.</li>
                    <br><br>
                </ul>

                <header class="section-header">
                    <h4><strong>7. Indemnification</strong></h4>
                </header>

                <ul>
                    <li>You agree to indemnify and hold harmless us, our affiliates, and each of our and their respective directors, officers, managers, employees, shareholders, agents, representatives and licensors, from and against any and all losses, expenses, damages and costs, including reasonable attorneys' fees, that arise out of your use of the Website, violation of these Terms by you or any other person using your account, or your violation of any rights of another. We reserve the right to take over the exclusive defense of any claim for which we are entitled to indemnification under this section. In such event, you agree to provide us with such cooperation as is reasonably requested by us.</li>
                    <br><br>
                </ul>

                <header class="section-header">
                    <h4><strong>8. Restrictions</strong></h4>
                    <h6>The User shall not be permitted or allowed to perform the following activities:</h6>
                </header>

                <ul>
                    <li>remove any copyright, trademark or other proprietary notices from any portion of the Content; </li>
                    <br>
                    <li>reproduce, modify, prepare derivative works based upon, distribute, license, lease, sell, resell, transfer, publicly display, publicly perform, transmit, stream, broadcast or otherwise exploit the Content except as expressly permitted by the Company; </li>
                    <br>
                    <li>decompile, reverse engineer or disassemble the Content except as may be permitted by applicable law; </li>
                    <br>
                    <li>link to, mirror or frame any portion of the Content; </li>
                    <br>
                    <li>cause or launch any programs or scripts for the purpose of scraping, indexing, surveying, or otherwise data mining any portion of the Content or unduly burdening or hindering the operation and/or functionality of any aspect of the Content; or </li>
                    <br>
                    <li>attempt to gain unauthorized access to or impair any aspect of the Content or its related systems or networks.</li>
                    <br><br>
                </ul>

                <header class="section-header">
                    <h4><strong>9. Intellectual Property Rights</strong></h4>
                </header>

                <ul>
                    <li>9.1.  The Company is the sole owner or lawful licensee of all the rights to the Website and its Content. The Website Content shall include but not be limited to, its design, layout, text, images, graphics, sound, video etc. The website content embodies trade secrets and intellectual property rights protected under worldwide copyright and other laws. All title, ownership and intellectual property rights in the Website and its content shall remain with the Company, its affiliates or licensors of the Company's Content, as the case may be.</li>
                    <br>
                    <li>9.2.  All rights, not otherwise claimed under these Terms are hereby reserved. The information contained in this Website is intended, solely to provide general information for the personal use of the User, who accepts full responsibility for its use. The Company does not represent or endorse the accuracy or reliability of any information, or advertisements or Content contained on, distributed through, or linked, downloaded or accessed from any of the Content contained on this Website, or the quality of any products, information or other materials displayed, or obtained by you as a result of an advertisement or any other information or offer in or in connection with the Content.</li>
                    <br>
                    <li>9.3.  The User shall not upload post or otherwise make available on the Website any material protected by copyright, trademark, patent, proprietary rights or any other intellectual property rights, without the express permission of the owner of the copyright, trademark, proprietary right or other intellectual property rights. </li>
                    <br>
                    <li>9.4.  The Company does not have any express burden or responsibility to provide the User with indications, markings or anything else that may aid the User in determining whether the material in question is copyrighted or trademarked. </li>
                    <br>
                    <li>9.5.  The User shall be solely liable for any damage resulting from any infringement of copyrights, trademarks, proprietary rights or any other harm resulting from such a submission.</li>
                    <br>
                    <li>9.6.  By submitting material to any public area of the Website, the User warrants that the owner of such material has expressly granted the Company the royalty-free, perpetual, irrevocable, non-exclusive right and license to use, reproduce, modify, adapt, publish, translate and distribute such material (in whole or in part) worldwide and/or to incorporate it in other works in any form, media or technology now known or hereafter developed for the full term of any copyright that may exist in such material. </li>
                    <br>
                    <li>9.7.  The User hereby grants the Company, the right to edit, copy, publish and distribute any material made available on the Website by the User. </li>
                    <br><br>
                </ul>

                <header class="section-header">
                    <h4><strong>10. Trademark</strong></h4>
                </header>

                <ul>
                    <li>10.1. All related icons and logos are registered trademarks or trademarks or service marks of the Company in various jurisdictions and are protected under applicable copyright, Trademark and other proprietary rights laws. The unauthorized copying, modification, use or publication of these marks is strictly prohibited.</li>
                    <br>
                    <li>10.2. The trademarks, logos and service marks ("<strong>Marks</strong>") displayed on the Website are the property of the Company and other respective persons. The User is prohibited from using any Marks for any purpose including, but not limited to use as metatags on other pages or sites on the World Wide Web without the written permission of the Company or such third party which may own the Marks. </li>
                    <br>
                    <li>10.3. Trademarks that are located within or on the Website or a website otherwise owned or operated in conjunction with the Company shall not be deemed to be in the public domain but rather the exclusive property of the Company, unless such site is under license from the trademark owner thereof, in which case such license is for the exclusive benefit and use of the Company, unless otherwise stated.</li>
                    <br><br>
                </ul>

                <header class="section-header">
                    <h4><strong>11. Miscellaneous</strong></h4>
                </header>

                <ul>
                    <li>11.1. Compliance: Failure to comply with these Terms and to any updated version will result in Legal actions taken for violations of applicable regulations like the information security laws and copyright regulations (including but not limited to Information Technology Act, 2000 and Information Technology (Reasonable security practices and procedures and sensitive personal data or information) Rules, 2011 and the Copy Right Act, 1957) and other applicable laws and regulations.</li>
                    <br>
                    <li>11.2. If any of these conditions are deemed invalid, void, or for any reason unenforceable, the parties agree that the court should endeavor to give effect to the parties’ intentions as reflected in the provision, and the unenforceable condition shall be deemed severable and shall not affect the validity and enforceability of any remaining condition. </li>
                    <br>
                    <li>11.3. The Terms and the relationship between the User and the Company will be governed by the laws as applicable in India. </li>
                    <br>
                    <li>11.4. Any disputes will be handled in the competent courts of Hyderabad, Telangana, India. </li>
                    <br>
                    <li>11.5. The failure of the Company to act with respect to a breach by the User or others does not waive its right to act with respect to subsequent or similar breaches. </li>
                    <br>
                    <li>11.6. Except as otherwise, expressly provided in these Terms, there shall be no third-party beneficiaries to the same. These Terms constitute the entire agreement between the User and the Company and governs the User’s use of the Website, superseding any prior agreements between the User and the Company with respect to the Website. </li>
                    <br><br>
                </ul>

                <header class="section-header">
                    <h4><strong>12. Contact</strong></h4>
                </header>

                <ul>
                    <li>In the event of any questions and grievances relating to this Website, please email us at ______________. </li>
                    <b><br>
                </ul>

            </div>



        </section>



    </main>

@endsection
@section('scripts')

@endsection
