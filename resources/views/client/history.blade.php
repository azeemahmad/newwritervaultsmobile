@extends('client.layouts.master')
<style>
.help-block{
  color: red;
}
</style>
@section('content')
  <main id="main">

    <!--==========================
    Why Us Section
    ============================-->
    <section id="why-us" class="outer custom-outer">

      @include('client.layouts.sub_header')

      <div class="container">



        <div class="row padding-tb">

          @include('client.layouts.side_menu')

          <div class="col-lg-9 custom-right-block-outer">

            <!-- MultiStep Form -->
            <div class="row padding-lr-15">
              <div class="custom-right-block">

                <div class="row">
                  <div class="col-lg-12 display-flex">
                    <div class="col-lg-9">
                      <h4 class="right-title">History</h4>
                      <h5 class="rsub-title">View all your registered documents through WritersVault.io at one place.</h5>
                    </div>

                  </div>
                </div>

                <div class="divider"></div>

                <div id="content" class="tab-content" role="tablist">
                  <div id="history" class="card tab-pane fade show active" role="tabpanel" aria-labelledby="registration">

                    <div id="collapse-A" class="collapse show" role="tabpanel" aria-labelledby="heading-A">
                      <div class="card-body">
                        <div class="container no-padding">
                          <table class="table table-fluid borderless" id="" border="0">

                          </table>

                          <table class="table table-fluid borderless" id="historytable" border="0">

                            <tbody>
                              <tr>
                                <td class="td-odd">Title</td>
                                <td class="td-even"><label>Date</label></td>
                                <td class="td-odd"><label>Download</label></td>
                              </tr>
                              @if(!empty($orders))
                                @foreach ($orders as $order)

                                  <tr>
                                    <td class="td-odd">{{ $order->title }}</td>
                                    <td class="td-even"><label>{{  $order->created_at }}</label></td>
                                    <td class="td-odd"><label><a Download href="{{ url('UserScripts/'.$order->file_type.'/'.$order->file1) }}">Download</a></label></td>
                                  </tr>
                                @endforeach
                              @endif
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- /.MultiStep Form -->


          </div>

        </div>

      </div>


      <div class="container">
        <div class="form-footer1 col-lg-3 offset-9">
          <div class=""><a href="{{ url('/terms') }}">Terms of use</a></div>
          <div class=""><a href="{{ url('/privacy') }}">Privacy policy</a></div>
        </div>
      </div>
    </section>
  </main>
@endsection
@section('scripts')
  <script>
  $('#tabs').find('.history').addClass('active');
  </script>
@endsection
