<div class="col-lg-3 no-padding">
    <div class="custom-left-block">

        <div class="upper-left">
            <div class="avatar-upload">
                <div class="avatar-edit">
                    <form id="datafiles" action="javascript:void(0)" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type='file' id="imageUpload" name="profile_image" accept=".png, .jpg, .jpeg" />
                        <label for="imageUpload"></label>
                    </form>
                </div>
                <div class="avatar-preview">
                    <div id="imagePreview">
                        @if(Auth::guard('client')->user()->image == null || Auth::guard('client')->user()->image =='')
                            <img src="{{asset('img/dummy.jpg')}}" style="width: 100%;" class="dummyimage" alt="">
                        @else
                            <img src="{{env('APP_URL').'/images/ClientProfile/'.Auth::guard('client')->user()->image}}" style="width: 193px;height: 195px;" class="dummyimage">
                        @endif
                    </div>
                </div>
            </div>
            <h6>{{Auth::guard('client')->user()->first_name.' '.Auth::guard('client')->user()->last_name}} - <strong>{{Auth::guard('client')->user()->membership_no}}</strong> </h6>
            {{-- @php
            $payment = Auth::guard('client')->user()->payment()->where('payment_status',1)->sum('amount');
            @endphp
            <p>{{ isset($payment) ? $payment:0 }} Credits</p> --}}

            <a class="update-kyc" href="#"><div class="scircle"> <img src="{{url('img/icons/u.png')}}"> </div><span class="ltitle">Update KYC</span></a>
        </div>

        <div class="clearfix"></div>

        <div class="lower-left">
            <ul id="tabs" class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a id="tab-A" href="{{url('/client/home')}}" class="nav-link registration"><div class="scircle"> <img src="{{url('img/icons/r.png')}}" alt=""> </div><label>Registration</label></a>
                </li>
                <li class="nav-item">
                    <a id="tab-B" href="{{url('/client/portfolio')}}" class="nav-link portfolio"><div class="scircle"> <img src="{{url('img/icons/p.png')}}" alt=""> </div><label>Portfolio</label></a>
                </li>
                <li class="nav-item">
                    <a id="tab-BC" href="{{url('/client/title')}}" class="nav-link title"><div class="scircle"> <img src="{{url('img/icons/v.png')}}" alt=""> </div><label>Title</label></a>
                </li>
                {{-- <li class="nav-item">
                    <a id="tab-C" href="{{url('/client/verification')}}" class="nav-link verification"><div class="scircle"> <img src="{{url('img/icons/v.png')}}" alt=""> </div><label>Verification</label></a>
                </li> --}}
                <li class="nav-item">
                    <a id="tab-C" href="{{url('/client/history')}}" class="nav-link history"><div class="scircle"> <img src="{{url('img/icons/v.png')}}" alt=""> </div><label>History</label></a>
                </li>
                <li class="nav-item">
                    <a id="tab-C" href="{{url('/client/help')}}" class="nav-link"><div class="scircle"> <img src="{{url('img/icons/h.png')}}" alt=""> </div><label>Help</label></a>
                </li>
            </ul>
        </div>

    </div>
</div>
