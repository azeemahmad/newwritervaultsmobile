<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
<footer id="footer" class="section-bg custom-footer">
    <div class="footer-top">
      <div class="container">

        <div class="row">

          <div class="col-lg-6 float-left">

            <div class="row">

                <div class="col-sm-6 float-left">

                  <div class="footer-info">
                    <p>SECURE YOUR WORK</p>
                  </div>

                  <div class="footer-newsletter">
                    @if(Auth::guard('client')->check())
                      <p><a href="{{'/client/home'}}"><strong>Register Upload Your KYC</strong></a></p>
                      @else
                    <p><a href="{{'client/register'}}"><strong>Register Upload Your KYC</strong></a></p>
                    @endif

                    <!-- <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem.</p> -->
                    <!-- <form action="" method="post">
                      <input type="email" name="email"><input type="submit"  value="Subscribe">
                    </form> -->
                  </div>

                </div>

                <div class="col-sm-6 float-left">
                  <div class="footer-links">
                    <p>SAY HELLO</p>
                      <div class="footer-newsletter">
                      <p>C Space HQ, 938a, Road No. 47, Silent Lake Valley, Jubilee Hills, Hyderabad-500033</p>

                    </div>
                  </div>

                </div>

            </div>

          </div>

          <div class="col-lg-6 float-left">

            <div class="row">

                <div class="col-sm-6 float-left">

                  <div class="footer-links">
                    <ul>
                      <li><a href="{{ url('/faq') }}">FAQs</a></li>
                      <li><a href="{{ url('/terms') }}">TERMS OF USE</a></li>
                      <li><a href="{{ url('/privacy') }}">PRIVACY POLICY</a></li>
                      {{--<li><a href="#">SITEMAP</a></li>--}}
                    </ul>
                  </div>

                </div>

                <div class="col-sm-6 float-left">
                  <div class="social-links">
                    <a href="#" class="instagram" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://twitter.com/writersvaultio" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.facebook.com/writersvaultio" class="facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                  </div>


                  <div class="footer-info">
                    <img src="{{url('img/logo.png')}}" class="logo">
                  </div>

                </div>

            </div>

          </div>



        </div>

      </div>
    </div>
  </footer>
