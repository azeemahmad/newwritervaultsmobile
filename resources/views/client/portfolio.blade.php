@extends('client.layouts.master')
<style>
.help-block{
  color: red;
}
</style>
@section('content')
  <main id="main">

    <!--==========================
    Why Us Section
    ============================-->
    <section id="why-us" class="outer custom-outer">

      @include('client.layouts.sub_header')

      <div class="container">
        <div class="row padding-tb">
          @include('client.layouts.side_menu')
          {{-- PUT MAIN CONTENT HERE START --}}

          <div class="col-lg-9 custom-right-block-outer">

            <!-- MultiStep Form -->
            <div class="row padding-lr-15">
              <div class="custom-right-block">

                <div class="row">
                  <div class="col-lg-12 display-flex">
                    <div class="col-lg-9">
                      <h4 class="right-title">Portfolio</h4>
                      <h5 class="rsub-title">View all your registered documents through WritersVault.io at one place.</h5>
                    </div>
                  </div>
              </div>
              <div class="divider"></div>
              <div id="content" class="tab-content" role="tablist">
                <div id="portfolio" class="card tab-pane fade show active custom-search-outer" role="tabpanel" aria-labelledby="registration">

                  <div id="collapse-A" class="collapse show" role="tabpanel" aria-labelledby="heading-A">
                    <div class="card-body">
                      <div class="container no-padding">

                        <div class="wrap customsearch">
                          <div class="search">
                            <input type="text" class="searchTerm" placeholder="Search your files by keyword">
                            <button type="submit" class="searchButton">
                              <div class="scircle"></div><i class="fa fa-search"></i>
                            </button>
                          </div>
                          <p>0 filters applied</p>
                        </div>

                        <div class="col-md-12 float-left folder-block">
                          <div class="container">
                            @foreach(App\Models\Scriptsfolder::orderBy('name')->where('status',1)->get() as $name)
                            <div class="col-md-2 float-left Show">
                              <a href="{{ url('client/title') }}/{{ $name->name }}">
                              <img src="{{ url('img/folder-icon.png')}}">
                              <p>{{ $name->name }}</p>
                              </a>
                            </div>
                          @endforeach
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.MultiStep Form -->
        </div>

      {{-- PUT MAIN CONTENT HERE END --}}
    </div>
  </div>

  <div class="container">
    <div class="form-footer1 col-lg-3 offset-9">
      <div class=""><a href="{{ url('/terms') }}">Terms of use</a></div>
      <div class=""><a href="{{ url('/privacy') }}">Privacy policy</a></div>
    </div>
  </div>
</section>
</main>
@endsection
@section('scripts')
  <script>
  $('#tabs').find('.portfolio').addClass('active');
  </script>
  @endsection
