@extends('client.layouts.master')
<style>
    .help-block{
        color: red;
    }
</style>
@section('content')
<main id="main" class="verification">

    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="outer custom-outer">

        @include('client.layouts.sub_header')
        <div class="container">

            <!-- <header class="section-header">
              <h3>Why choose us?</h3>
              <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus.</p>
            </header> -->

            <div class="row padding-tb">

                @include('client.layouts.side_menu')

                <div class="col-lg-9 custom-right-block-outer">

                    <!-- MultiStep Form -->
                    <div class="row padding-lr-15">
                        <div class="custom-right-block">

                            <div class="row">
                                <div class="col-lg-12 display-flex">
                                    <div class="col-lg-12">
                                        <h4 class="right-title">Verification</h4>
                                        <h5 class="rsub-title">Only a file previously Verifyed on CopyrightBank can be verified. You may upload the original file or an identical copy of it.</h5>
                                    </div>


                                </div>
                            </div>

                            <div class="divider"></div>

                            <div id="content" class="tab-content" role="tablist">
                                <div id="verification" class="card tab-pane fade show active" role="tabpanel" aria-labelledby="registration">

                                    <div id="collapse-A" class="collapse show" role="tabpanel" aria-labelledby="heading-A">
                                        <div class="card-body">
                                            <div class="container no-padding">
                                                <!-- <h2>Simple Pagination Example using Datatables Js Library</h2> -->
                                                <table class="table table-fluid borderless" id="verificationtable" border="0">
                                                    <thead>
                                                    <tr>
                                                        <th><div class="scircle"></div>&nbsp;File Name</th>
                                                        <th class="table-regtitle"><label class="">Verify All</label></th>
                                                        <th class="table-remtitle"><label>Remove All</label></th></tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>1. Draft Name 1</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2. Draft Name 2</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3. Draft Name 3</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>4. Draft Name 4</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>5. Draft Name 5</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>6. Draft Name 6</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>7. Draft Name 7</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>8. Draft Name 8</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9. Draft Name 9</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9. Draft Name 9</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9. Draft Name 9</td>
                                                        <td class="regtitle-td"><label>Verify</label></td>
                                                        <td class="remtitle-td"><label>Remove</label></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row padding-tb-15">
                        <div class="col-lg-12 display-flex">
                            <div class="col-lg-12">
                                <!-- <a class="add-file" href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Another File</a> -->
                                <div class="avatar-upload add-file-outer">
                                    <div class="avatar-edit add-file">
                                        <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                        <label for="imageUpload"><i class="fa fa-plus-circle" aria-hidden="true"></i>Verify Another File</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.MultiStep Form -->

                    <!-- <div class="form-footer col-lg-6 offset-3">
                        <div class=""><a href="#">Terms of use</a></div>
                        <div class=""><a href="#">Privacy policy</a></div>
                    </div> -->

                </div>

            </div>

        </div>



        <div class="container">
            <div class="form-footer1 col-lg-3 offset-9">
                <div class=""><a href="{{ url('/terms') }}">Terms of use</a></div>
                <div class=""><a href="{{ url('/privacy') }}">Privacy policy</a></div>
            </div>
        </div>


    </section>
</main>
    @endsection
