@extends('client.layouts.master')
<style>
    .help-block{
        color: red;
    }
</style>
@section('content')
    <main id="main">
        <section id="why-us" class="outer">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-6 no-padding">
                        <div class="why-us-img">
                            <img src="https://dummyimage.com/900x900/cccccc/000000" alt="" class="img-fluid">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-outer">
                            @if (session('failed_message'))
                                <p class="alert alert-danger">
                                    {{ session('failed_message') }}
                                </p>
                            @endif
                            @if (session('flash_message'))
                                <p class="alert alert-success">
                                    {{ session('flash_message') }}
                                </p>
                            @endif
                            <h2 class="main-title">WRITERS VAULT</h2>
                            <h5 class="sub-title">Welcome back! Please login to your account.</h5>
                            <form method="POST" action="{{ route('client.login') }}" role="form" class="col-lg-6 offset-3">
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="user_name" class="form-control" id="user_name" placeholder="Username" data-rule="minlen:4" data-msg="Enter Username" />
                                    @if ($errors->has('user_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" data-rule="password" data-msg="Please enter a valid Password" />
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group remember-outer">
                                    <div class="checkbox">
                                        <label><input checked="checked" name="remember" type="checkbox">&nbsp;&nbsp; Remember me</label>
                                    </div>

                                    <div class="forgot-password"><a href="{{url('/client/password/reset')}}">Forgot Password</a></div>
                                </div>

                                <!-- <div id="sendmessage">Your message has been sent. Thank you!</div>
                                <div id="errormessage"></div> -->

                                <div class="action-btn-outer">
                                    <div class="text-center"><button type="submit" title="Send Message" class="submit-btn">Login</button></div>
                                    <div class="text-center"><a href="{{url('/client/register')}}"> <button type="button" title="Sign up" class="submit-btn signup-btn">Sign up</button></a></div>
                                </div>
                            </form>

                        </div>

                        <div class="form-footer col-lg-6 offset-3">
                            <div class=""><a href="{{ url('/terms') }}">Terms of use</a></div>
                            <div class=""><a href="{{ url('/privacy') }}">Privacy policy</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

@endsection
@section('scripts')
<script type="text/javascript">
         $('.custom-carousel').owlCarousel({
      autoplay: true,
      center: true,
      loop: true,
      nav: false
    });
    </script>
@endsection
