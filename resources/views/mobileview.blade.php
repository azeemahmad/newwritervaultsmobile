
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Writers Vault</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">


    <style type="text/css">


    </style>
</head>

<body>
<!--==========================
Header
============================-->
<header id="header">

    <!-- <div id="topbar">
      <div class="container">
        <div class="social-links">
          <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
          <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
          <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
          <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
        </div>
      </div>
    </div> -->

    <div class="container">

        <div class="logo float-lef">
            <!-- Uncomment below if you prefer to use an image logo -->
            <h1 class="text-light" style="background: none;text-align: center;margin: 0 auto;display: block;"><a href="#intro" class="scrollto"><img src="{{asset('img/logo.png')}}"></a></h1>
            <!-- <a href="#header" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a> -->
        </div>



    </div>
</header><!-- #header -->

<!--==========================
  Intro Section
============================-->
<section id="intro" class="clearfix" style="background: none;">
    <div class="container d-flex h-100">
        <div class="row justify-content-center align-self-center" style="margin: 0px;width: 100% !important;max-width: 100% !important;">
            <div class="col-md-7 intro-info order-md-first order-last">
                <img src="{{asset('img/mobile.png')}}"><br>
                <h2 style="text-align: center;padding-top: 50px;font-size: 25px !important;">Mobile Version <br>Will Be Coming Soon</h2>
                <h4 style="    font-size: 20px !important;">Kindly visit the website on your desktop</h4>

            </div>


        </div>

    </div>
</section>

<main id="main">





</main>


<!-- <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a> -->
<!-- Uncomment below i you want to use a preloader -->
<!-- <div id="preloader"></div> -->

<!-- JavaScript Libraries -->



</body>
</html>
