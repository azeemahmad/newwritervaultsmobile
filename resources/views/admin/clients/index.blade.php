@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading">Clients</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/clients/create') }}" class="btn btn-success btn-sm"
                           title="Add New Client">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/clients', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Image</th>
                                    <th>Status</th> @can('view_clients','edit_clients', 'delete_clients')
                                    <th class="text-center">Actions</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($clients as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->first_name }}</td>
                                        <td>{{ $item->last_name }}</td>
                                        <td>{{ $item->user_name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>
                                               @if($item->image == null ||$item->image == '' )
                                                <img src="{{asset('img/dummy.jpg')}}" style="width:40px; height:60px;" class="dummyimage" alt="">
                                                @else
                                                @php $imageProfile=$item->image;@endphp
                                                <img src="{{env('APP_URL').'/images/ClientProfile/'.$imageProfile}}" style="width:40px; height:60px;" class="dummyimage">
                                            @endif
                                        </td>
                                        <td>{{ $item->status==1?'Active':'In-Active' }}</td>
                                        @can('view_clients')
                                        <td class="text-center">
                                            <a href="{{ url('/admin/clients/' . $item->id) }}" title="View Client">
                                                <button class="btn btn-primary btn-xs"><i class="fa fa-eye"
                                                                                          aria-hidden="true"></i> View
                                                </button>
                                            </a>
                                            @include('shared._actions', ['entity' => 'clients',
                                             'id' => $item->id
                                             ])
                                        </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $clients->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
