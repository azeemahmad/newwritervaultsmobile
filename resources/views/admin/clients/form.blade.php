<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Client</b></div>
        <br/>
         <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
    <label for="first_name" class="col-md-2 control-label">{{ 'First Name' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="first_name" type="text" id="first_name" value="{{ isset($client->first_name) ? $client->first_name : ''}}" required>
    {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
    <label for="last_name" class="col-md-2 control-label">{{ 'Last Name' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="last_name" type="text" id="last_name" value="{{ isset($client->last_name) ? $client->last_name : ''}}" >
    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('user_name') ? 'has-error' : ''}}">
    <label for="user_name" class="col-md-2 control-label">{{ 'User Name' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="user_name" type="text" id="user_name" value="{{ isset($client->user_name) ? $client->user_name : ''}}" required>
    {!! $errors->first('user_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="col-md-2 control-label">{{ 'Email' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="email" type="email" id="email" value="{{ isset($client->email) ? $client->email : ''}}" required>
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="col-md-2 control-label">{{ 'Password' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="password" type="password" id="password" >
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-md-2 control-label">{{ 'Status' }} :</label>
    <div class="col-md-8">
     <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($client->status) && $client->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
          <div class="form-group">
                <div class="col-md-offset-4 col-md-6">
                      <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
                </div>
          </div>
     </div>
</div>
