@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-success">
                    <div class="panel-heading">Client {{ $client->id }}</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/clients') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>
                        @can('edit_clients', 'delete_clients')
                        <a href="{{ url('/admin/clients/' . $client->id . '/edit') }}" title="Edit Client">
                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> Edit
                            </button>
                        </a>

                        <form method="POST" action="{{ url('admin/clients' . '/' . $client->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Client"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i>
                                Delete
                            </button>
                        </form>
                        @endcan
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $client->id }}</td>
                                </tr>
                                <tr>
                                    <th> First Name</th>
                                    <td> {{ $client->first_name }} </td>
                                </tr>
                                <tr>
                                    <th> Last Name</th>
                                    <td> {{ $client->last_name }} </td>
                                </tr>
                                <tr>
                                    <th> User Name</th>
                                    <td> {{ $client->user_name }} </td>
                                </tr>
                                <tr>
                                    <th> Email</th>
                                    <td> {{ $client->email }} </td>
                                </tr>
                                <tr>
                                    <th> Image</th>
                                    <td>
                                        @if($client->image == null ||$client->image == '' )
                                            <img src="{{asset('img/dummy.jpg')}}" style="width: 120px; height: 150px" class="dummyimage" alt="">
                                        @else
                                            @php $imageProfile=$client->image;@endphp
                                            <img src="{{env('APP_URL').'/images/ClientProfile/'.$imageProfile}}" style="width: 120px; height: 150px" class="dummyimage">
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td> {{ $client->status==1?'Active':'In-Active' }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
