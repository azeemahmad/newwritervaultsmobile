@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading">Clientregisterations</div>
                    <div class="panel-body">
                        {{--<a href="{{ url('/admin/clientregisterations/create') }}" class="btn btn-success btn-sm"--}}
                           {{--title="Add New Clientregisteration">--}}
                            {{--<i class="fa fa-plus" aria-hidden="true"></i> Add New--}}
                        {{--</a>--}}

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/clientregisterations', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Client Name</th>
                                    <th>Client Email</th>
                                    <th>Total File Upload</th>
                                    <th>Success Upload</th>
                                    <th>View Files</th>
                                    {{--</th> @can('view_clientregisterations','edit_clientregisterations', 'delete_clientregisterations')--}}
                                    {{--<th class="text-center">Actions</th>--}}
                                    {{--@endcan--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($clientregisterations as $key => $item)
                                    <tr>
                                        <?php
                                            $totalOrder=App\Models\Clientregisteration::where('client_id',$item->client_id)->count();
                                            $successOrder=App\Models\Clientregisteration::where('client_id',$item->client_id)->where('order_status','PAYMENT_COMPLETED')->count();
                                                ?>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ isset($item->client->first_name)?$item->client->first_name.' '.$item->client->first_name:'' }}</td>
                                        <td>{{ isset($item->client->email)?$item->client->email:'' }}</td>
                                        <td>{{$totalOrder }}</td>
                                        <td>{{ $successOrder}}</td>
                                        <td><a href="{{ url('/admin/clientregisterations/viewsallfiles/' . $item->client_id) }}"><button class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a></td>
                                        {{--@can('view_clientregisterations')--}}
                                        {{--<td class="text-center">--}}
                                            {{--<a href="{{ url('/admin/clientregisterations/' . $item->id) }}"--}}
                                               {{--title="View Clientregisteration">--}}
                                                {{--<button class="btn btn-primary btn-xs"><i class="fa fa-eye"--}}
                                                                                          {{--aria-hidden="true"></i> View--}}
                                                {{--</button>--}}
                                            {{--</a>--}}
                                            {{--@include('shared._actions', ['entity' => 'clientregisterations',--}}
                                             {{--'id' => $item->id--}}
                                             {{--])--}}
                                        {{--</td>--}}
                                        {{--@endcan--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $clientregisterations->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
