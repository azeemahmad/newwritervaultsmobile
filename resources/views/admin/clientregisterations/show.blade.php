@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-success">
                    <div class="panel-heading">Clientregisteration {{ $clientregisteration->id }}</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/clientregisterations') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        @can('edit_clientregisterations', 'delete_clientregisterations')
                        <a href="{{ url('/admin/clientregisterations/' . $clientregisteration->id . '/edit') }}" title="Edit Clientregisteration"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/clientregisterations' . '/' . $clientregisteration->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Clientregisteration" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $clientregisteration->id }}</td>
                                    </tr>
                                    <tr><th> Client Id </th><td> {{ $clientregisteration->client_id }} </td></tr><tr><th> Auther Name </th><td> {{ $clientregisteration->auther_name }} </td></tr><tr><th> Co Auther Name </th><td> {{ $clientregisteration->co_auther_name }} </td></tr><tr><th> File Type </th><td> {{ $clientregisteration->file_type }} </td></tr><tr><th> Title </th><td> {{ $clientregisteration->title }} </td></tr><tr><th> File1 </th><td> {{ $clientregisteration->file1 }} </td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
