@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading">View All Files</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/clientregisterations') }}" class="btn btn-success btn-sm"
                        title="Add New Clientregisteration">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>

                        {!! Form::open(['class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Author Name</th>
                                    <th>Title</th>
                                    <th>File Type</th>
                                    <th>Order Total</th>
                                    <th>Payment Status</th>
                                    <th>Date</th>
                                    <th>View Files</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($clientRegistartion as $key => $item)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$item->auther_name}}</td>
                                        <td>{{ucwords($item->title)}}</td>
                                        <td>{{ucwords($item->file_type)}}</td>
                                        <td>{{$item->order_total}}</td>
                                        @if($item->order_status=='PAYMENT_COMPLETED')
                                            <td style="color: green">{{$item->order_status}}</td>
                                         @else
                                            <td style="color: red">{{$item->order_status}}</td>
                                        @endif
                                        <td>{{date('d M Y',strtotime($item->created_at))}}</td>
                                        <td>
                                            <a href="{{ asset('UserScripts/'.$item->file_type.'/'.$item->file1)}}" target="_blank" class="btn btn-info btn-md"
                                               title="Add New Clientregisteration">
                                                <i class="fa fa-eye" aria-hidden="true"></i> view
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $clientRegistartion->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.navbar-form').submit(function(){

                return false;

            });
        });
    </script>
    @endsection
