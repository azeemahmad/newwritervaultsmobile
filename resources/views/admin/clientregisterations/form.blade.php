<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Clientregisteration</b></div>
        <br/>
         <div class="form-group {{ $errors->has('client_id') ? 'has-error' : ''}}">
    <label for="client_id" class="col-md-2 control-label">{{ 'Client Id' }} :</label>
    <div class="col-md-8">
     <select name="client_id" class="form-control" id="client_id" required>
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($clientregisteration->client_id) && $clientregisteration->client_id == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('client_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('auther_name') ? 'has-error' : ''}}">
    <label for="auther_name" class="col-md-2 control-label">{{ 'Auther Name' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="auther_name" type="text" id="auther_name" value="{{ isset($clientregisteration->auther_name) ? $clientregisteration->auther_name : ''}}" required>
    {!! $errors->first('auther_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('co_auther_name') ? 'has-error' : ''}}">
    <label for="co_auther_name" class="col-md-2 control-label">{{ 'Co Auther Name' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="co_auther_name" type="text" id="co_auther_name" value="{{ isset($clientregisteration->co_auther_name) ? $clientregisteration->co_auther_name : ''}}" >
    {!! $errors->first('co_auther_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('file_type') ? 'has-error' : ''}}">
    <label for="file_type" class="col-md-2 control-label">{{ 'File Type' }} :</label>
    <div class="col-md-8">
     <select name="file_type" class="form-control" id="file_type" required>
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($clientregisteration->file_type) && $clientregisteration->file_type == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('file_type', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="col-md-2 control-label">{{ 'Title' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="title" type="text" id="title" value="{{ isset($clientregisteration->title) ? $clientregisteration->title : ''}}" >
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('file1') ? 'has-error' : ''}}">
    <label for="file1" class="col-md-2 control-label">{{ 'File1' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="file1" type="file" id="file1" value="{{ isset($clientregisteration->file1) ? $clientregisteration->file1 : ''}}" >
    {!! $errors->first('file1', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('file2') ? 'has-error' : ''}}">
    <label for="file2" class="col-md-2 control-label">{{ 'File2' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="file2" type="file" id="file2" value="{{ isset($clientregisteration->file2) ? $clientregisteration->file2 : ''}}" >
    {!! $errors->first('file2', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('file3') ? 'has-error' : ''}}">
    <label for="file3" class="col-md-2 control-label">{{ 'File3' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="file3" type="file" id="file3" value="{{ isset($clientregisteration->file3) ? $clientregisteration->file3 : ''}}" >
    {!! $errors->first('file3', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-md-2 control-label">{{ 'Status' }} :</label>
    <div class="col-md-8">
     <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($clientregisteration->status) && $clientregisteration->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
          <div class="form-group">
                <div class="col-md-offset-4 col-md-6">
                      <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
                </div>
          </div>
     </div>
</div>
