<style>
    li.select2-selection__choice {
        color: black !important;
        background-color: lightblue !important;
    }
    #permissionHeading{
        margin-left: 284px;
        padding-bottom: 15px;
    }

</style>
<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Users</b></div>
        <br/>

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="name" class="col-md-4 control-label">{{ 'Name' }}</label>

            <div class="col-md-6">
                <input class="form-control" name="name" type="text" id="name"
                       value="{{ isset($user->name) ? $user->name : ''}}">
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            <label for="email" class="col-md-4 control-label">{{ 'Email' }}</label>

            <div class="col-md-6">
                <input class="form-control" name="email" type="email" id="email"
                       value="{{ isset($user->email) ? $user->email : ''}}">
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
            <label for="password" class="col-md-4 control-label">{{ 'Password' }}</label>
            <div class="col-md-6">
                <input class="form-control" name="password" type="password" id="password"
                       value="">
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
            </div>
        </div>


        <div class="form-group {{ $errors->has('roles') ? 'has-error' : ''}}">
            <label for="roles" class="col-md-4 control-label">{{ 'Roles' }}</label>
            <div class="col-md-6">
                <b style="color: red;">Note:- </b><i>Select one or more than one roles.</i>
                @if(isset($rol))
                    {!! Form::select('roles[]', $rol, isset($user) ? $user->roles->pluck('id')->toArray() : null,  ['class' => 'form-control', 'id' =>'userlistselect', 'multiple']) !!}
                @else
                    <select name="roles[]" class="form-control" id="userlistselect" multiple>
                        @if(isset($roles) && !empty($roles))
                            @foreach($roles as $key => $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>
                            @endforeach
                        @endif
                    </select>
               @endif
                {!! $errors->first('roles', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        @if(isset($user))
            @include('shared._permissions', ['closed' => 'true', 'model' => $user ])
        @endif
        <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
                <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
            </div>
        </div>
    </div>
</div>
@section('scripts')
<script>
    $(document).ready(function() {
       $('#userlistselect').select2();
    });
</script>
@endsection