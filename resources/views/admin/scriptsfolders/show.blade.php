@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-success">
                    <div class="panel-heading">Scriptsfolder {{ $scriptsfolder->id }}</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/scriptsfolders') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        @can('edit_scriptsfolders', 'delete_scriptsfolders')
                        <a href="{{ url('/admin/scriptsfolders/' . $scriptsfolder->id . '/edit') }}" title="Edit Scriptsfolder"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/scriptsfolders' . '/' . $scriptsfolder->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Scriptsfolder" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $scriptsfolder->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $scriptsfolder->name }} </td></tr><tr><th> Status </th><td> {{ $scriptsfolder->status }} </td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
