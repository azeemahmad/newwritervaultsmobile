@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading">Scriptsfolders</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/scriptsfolders/create') }}" class="btn btn-success btn-sm" title="Add New Scriptsfolder">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/scriptsfolders', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <thead>
                                    <tr>
                                        <th>Sr.no</th><th>Name</th><th>Status</th> @can('view_scriptsfolders','edit_scriptsfolders', 'delete_scriptsfolders')
                                                                                                              <th class="text-center">Actions</th>
                                                                                                              @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($scriptsfolders as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->name }}</td><td>{{ $item->status==1?'Active':'In-Active' }}</td>
                                        @can('view_scriptsfolders')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/scriptsfolders/' . $item->id) }}" title="View Scriptsfolder"><button class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                        @include('shared._actions', ['entity' => 'scriptsfolders',
                                         'id' => $item->id
                                         ])
                                         </td>
                                         @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $scriptsfolders->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
