<header class="main-header">

    <!-- Logo -->
    <a href="#" class="logo"><b>Writers Vault</b></a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        @if(Auth::guard('web')->user()->image)
                            <img src="{{asset('images/profile_image'.'/'.Auth::guard('web')->user()->image)}}" class="user-image" alt="User Image"/>
                        @else
                            <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="user-image" alt="User Image"/>
                            @endif

                                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ ucwords(Auth::guard('web')->user()->name)}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            @if(Auth::guard('web')->user()->image)
                                <img src="{{asset('images/profile_image'.'/'.Auth::guard('web')->user()->image)}}" class="img-circle" alt="User Image"/>
                            @else
                                <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image"/>
                            @endif
                            <p>
                                {{ ucwords(Auth::guard('web')->user()->name)}} - Admin User

                            </p>
                        </li>

                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{url('/admin/profile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{url('/admin/logout')}}" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>