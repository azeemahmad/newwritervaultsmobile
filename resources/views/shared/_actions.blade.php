@can('edit_'.$entity)
<a href="{{ route($entity.'.edit', [str_singular($entity) => $id])  }}" class="btn btn-xs btn-info">
    <i class="fa fa-pencil-square-o" aria-hidden="true"> Edit</i></a>
@endcan

@can('delete_'.$entity)
{!! Form::open( ['method' => 'delete', 'url' => route($entity.'.destroy', ['user' => $id]), 'style' => 'display: inline', 'onSubmit' => 'return confirm("Are yous sure wanted to delete it?")']) !!}
<button type="submit" class="btn btn-xs btn-danger">
    <i class="fa fa-trash" aria-hidden="true"> Delete</i>
</button>
{!! Form::close() !!}
@endcan
